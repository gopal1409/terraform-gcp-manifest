terraform {
  required_providers {
    google = {
        source = "hashicorp/google"
    }
  }
}

provider "google" {
    ##project name you will get it from the json file
  project = ""
  region = "us-central-1"
  zone = "us-cental-1a"
  credentials = "key.json"
}


resource "google_storage_bucket" "bucket_from_username" {
  name = "bucket-from-username"
  location = "us-central"
}